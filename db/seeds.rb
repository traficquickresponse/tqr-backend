# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

JobPosition.create!([
                      {id: 'fa31101c-b9d1-4709-905b-1c784402f904', title: "BRIGADIR"},
                      {id: 'f8e59990-3697-4012-b018-58cac19bdc99', title: "BRIPKA"},
                      {id: 'dcfea5ee-22ff-45f9-a733-e3199cb2cdf3', title: "BRIPTU"},
                      {id: '8eed3bac-505c-4684-af3a-7285dc5f0942', title: "AIPTU"},
                      {id: '106ea5d0-77c7-4ba4-ad94-691835d6a80e', title: "BRIPDA"},
                    ])
CaseType.create!([
                   {id: '5c52484e-029a-4c95-808a-50dea900866d', title: "KECELAKAAN", event_name: "kecelakaan"},
                   {id: 'fba82dcd-e575-4c74-bf2d-1a58c85fc62e', title: "KEMACETAN", event_name: "kemacetan"},
                   {id: '08169bee-85b7-4ee5-ad25-2f271aaa48a4', title: "PLB", event_name: "plb"},
                   {id: 'c5c6c6ad-c131-4ded-9eba-cee9e7570831', title: "APEL PERS", event_name: "apel_pers"},
                 ])
@department = Department.create!(
  {id: '53530c6c-5401-469d-95a8-c2cdd060c80e', title: "Huru Hara"},
)

Setting.create(update_interval: 10, update_radius: 5, is_monitoring: true)

@roni                     = User.create!({
                                           id:                   "34cc9805-a148-4108-a6c8-6c24da951ce7",
                                           name:                 "Roni",
                                           phone_number:         "+621234567890",
                                           nrp_number:           "1234567890",
                                           job_position_id:      "dcfea5ee-22ff-45f9-a733-e3199cb2cdf3",
                                           authentication_token: "WSBkH5GTVpAD44eW2MESAb8n",
                                           channel:              "50015b2925ee16ab",
                                           department_id:        @department.id,
                                           password_digest:      "$2a$10$9a28gCAzdiXbz84UiymU5.h6j3v/5oyUeivqoDNocdvsPmDqxYmqe",
                                         })
@roni.authentication_token= "WSBkH5GTVpAD44eW2MESAb8n"
@roni.channel             = "50015b2925ee16ab"
@roni.password_digest     = "$2a$10$RdjDXh8VgpRbstN2RDUVZeUX9opgbUIbEp7zm9PzySTCgIQatuv3G"
@roni.type                = "Admin"
@roni.save(validate: false)
