# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160918163500) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "case_assigns", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "open_case_id"
    t.uuid     "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "checkin_at"
    t.datetime "readed_at"
    t.string   "description"
  end

  create_table "case_types", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "event_name"
    t.index ["event_name"], name: "index_case_types_on_event_name", using: :btree
  end

  create_table "departments", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_positions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "open_cases", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "location"
    t.text     "description"
    t.uuid     "case_type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.decimal  "lat"
    t.decimal  "lng"
    t.datetime "close_case_at"
  end

  create_table "settings", force: :cascade do |t|
    t.integer  "update_interval"
    t.integer  "update_radius"
    t.boolean  "is_monitoring"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "user_settings", force: :cascade do |t|
    t.string   "settingable_type"
    t.string   "settingable_id"
    t.integer  "update_interval"
    t.integer  "update_radius"
    t.boolean  "is_monitoring",    default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "nrp_number"
    t.uuid     "job_position_id"
    t.string   "authentication_token"
    t.string   "channel"
    t.string   "online_status"
    t.string   "password_digest"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.decimal  "lat"
    t.decimal  "lng"
    t.boolean  "is_online",            default: false
    t.uuid     "department_id"
    t.string   "type",                 default: "User"
    t.string   "iid_token"
    t.datetime "non_active_at"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
    t.index ["channel"], name: "index_users_on_channel", unique: true, using: :btree
    t.index ["nrp_number"], name: "index_users_on_nrp_number", unique: true, using: :btree
    t.index ["password_digest"], name: "index_users_on_password_digest", using: :btree
  end

end
