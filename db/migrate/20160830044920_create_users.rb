class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, id: :uuid do |t|
      t.string :name
      t.string :phone_number
      t.string :nrp_number
      t.uuid :job_position_id
      t.string :authentication_token
      t.string :channel
      t.string :online_status
      t.string :password_digest

      t.timestamps
    end
    add_index :users, :channel, unique: true
    add_index :users, :authentication_token, unique: true
    add_index :users, :nrp_number, unique: true
    add_index :users, :password_digest
  end
end
