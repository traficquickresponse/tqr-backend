class AddDepartmentIdToAdmins < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :department_id, :uuid
  end
end
