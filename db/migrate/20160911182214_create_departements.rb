class CreateDepartements < ActiveRecord::Migration[5.0]
  def change
    create_table :departements , id: :uuid do |t|
      t.string :title

      t.timestamps
    end
  end
end
