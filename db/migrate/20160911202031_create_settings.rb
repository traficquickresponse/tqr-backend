class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.string :update_interval
      t.string :update_radius
      t.boolean :is_monitoring

      t.timestamps
    end
  end
end
