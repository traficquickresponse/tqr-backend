class AddIsAdminToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :type, :string, default: "User"
    drop_table :admins
  end
end
