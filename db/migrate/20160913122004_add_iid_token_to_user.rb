class AddIidTokenToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :iid_token, :string
  end
end
