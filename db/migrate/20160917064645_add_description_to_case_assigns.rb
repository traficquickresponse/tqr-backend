class AddDescriptionToCaseAssigns < ActiveRecord::Migration[5.0]
  def change
    add_column :case_assigns, :description, :string
  end
end
