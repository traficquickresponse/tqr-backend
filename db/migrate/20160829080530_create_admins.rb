class CreateAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :admins , id: :uuid do |t|
      t.string :password_digest
      t.string :name
      t.string :nrp_number
      t.uuid :job_position_id
      t.string :channel
      t.string :authentication_token

      t.timestamps
    end
    add_index :admins, :channel, unique: true
    add_index :admins, :authentication_token, unique: true
    add_index :admins, :nrp_number, unique: true
    add_index :admins, :password_digest
  end
end
