class CreateUserSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :user_settings do |t|
      t.string :settingable_type
      t.string :settingable_id
      t.string :update_interval
      t.string :update_radius
      t.boolean :is_monitoring, default: false

      t.timestamps
    end
  end
end
