class AddFieldInCaseAssign < ActiveRecord::Migration[5.0]
  def change
    add_column :case_assigns, :checkin_at, :datetime
    add_column :case_assigns, :readed_at, :datetime
  end
end
