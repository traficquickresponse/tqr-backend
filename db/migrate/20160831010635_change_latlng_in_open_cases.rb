class ChangeLatlngInOpenCases < ActiveRecord::Migration[5.0]
  def change
    remove_column  :open_cases, :lat
    remove_column  :open_cases, :lng
    add_column  :open_cases, :lat, :decimal
    add_column  :open_cases, :lng, :decimal
  end
end
