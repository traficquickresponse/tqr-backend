class AddEventNameToCaseType < ActiveRecord::Migration[5.0]
  def change
    add_column :case_types, :event_name, :string
    add_index :case_types, :event_name

  end
end
