class CreateJobPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :job_positions , id: :uuid do |t|
      t.string :title

      t.timestamps
    end
  end
end
