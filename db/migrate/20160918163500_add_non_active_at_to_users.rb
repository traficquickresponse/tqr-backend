class AddNonActiveAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :non_active_at, :datetime
  end
end
