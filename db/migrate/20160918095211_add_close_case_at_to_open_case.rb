class AddCloseCaseAtToOpenCase < ActiveRecord::Migration[5.0]
  def change
    add_column :open_cases, :close_case_at, :datetime
  end
end
