class AddDepartementIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :departement_id, :uuid
  end
end
