class FixTypoDepartment < ActiveRecord::Migration[5.0]
  def change
    rename_table :departements, :departments
    rename_column :users, :departement_id, :department_id
  end
end
