class CreateCaseAssigns < ActiveRecord::Migration[5.0]
  def change
    create_table :case_assigns , id: :uuid do |t|
      t.uuid :open_case_id
      t.uuid :user_id

      t.timestamps
    end
  end
end
