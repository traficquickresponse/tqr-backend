class CreateOpenCases < ActiveRecord::Migration[5.0]
  def change
    create_table :open_cases, id: :uuid do |t|
      t.string :location
      t.decimal :lat, :precision => 8, :scale => 2
      t.decimal :lng, :precision => 8, :scale => 2
      t.text :description
      t.uuid :case_type_id

      t.timestamps
    end
  end
end
