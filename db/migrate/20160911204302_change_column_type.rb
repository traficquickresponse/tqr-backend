class ChangeColumnType < ActiveRecord::Migration[5.0]
  def change
    change_column :user_settings, :update_interval, 'integer USING CAST(update_interval AS integer)'
    change_column :user_settings, :update_radius, 'integer USING CAST(update_radius AS integer)'

    change_column :settings, :update_interval, 'integer USING CAST(update_interval AS integer)'
    change_column :settings, :update_radius, 'integer USING CAST(update_radius AS integer)'

  end
end
