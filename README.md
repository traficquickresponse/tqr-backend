## Build Setup

``` bash

# install dependencies
bundle install

# serve with hot reload at localhost:3000
rails server

```
