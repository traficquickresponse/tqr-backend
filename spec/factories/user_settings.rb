FactoryGirl.define do
  factory :user_setting do
    update_interval "MyString"
    update_radius "MyString"
    is_monitoring false
  end
end
