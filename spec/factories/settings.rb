FactoryGirl.define do
  factory :setting do
    update_interval "MyString"
    update_radius "MyString"
    is_monitoring false
  end
end
