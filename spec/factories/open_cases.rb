FactoryGirl.define do
  factory :open_case do
    location "MyString"
    lat "9.99"
    lng "9.99"
    description "MyText"
    case_type_id ""
  end
end
