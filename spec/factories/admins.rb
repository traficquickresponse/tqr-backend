FactoryGirl.define do
  factory :admin do
    password_digest "MyString"
    name "MyString"
    nrp_number "MyString"
    online_status "MyString"
    job_position_id ""
    channel "MyString"
  end
end
