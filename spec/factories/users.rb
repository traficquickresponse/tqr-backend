FactoryGirl.define do
  factory :user do
    name "MyString"
    phone_number "MyString"
    nrp_number "MyString"
    job_position_id ""
    authentication_token "MyString"
    channel "MyString"
    online_status "MyString"
  end
end
