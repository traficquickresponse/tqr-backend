class User < ApplicationRecord
  has_secure_password
  has_secure_token :authentication_token

  belongs_to :job_position
  belongs_to :department
  has_many :case_assigns, dependent: :destroy
  has_many :my_cases, :through => :case_assigns, :source => 'open_case'
  has_one :user_setting, as: :settingable
  has_one :setting, as: :settingable , class_name: 'UserSetting', dependent: :destroy

  def setting
    super || Setting.last
  end
  validates :job_position_id, presence: true
  validates :department_id, presence: true
  validates :nrp_number, presence: true
  validates :name, presence: true
  validates :name, format: {
      with:        /\A[.,a-zA-Z\s]+\z/,
      message:     "Format nama tidak valid!",
      allow_blank: false}
  validates :nrp_number, uniqueness: true
  validates :nrp_number, length: {minimum: 5}
  validates :phone_number, uniqueness: true
  VALID_PHONE_REGEX = /\A^(?:\+62?\d\s*-?)?\(?(?:\d{3})?\)?\d?\d{8}$+\z/i
  validates :phone_number, presence: true, length: {maximum: 20},
            format:                  {with: VALID_PHONE_REGEX}, uniqueness: {case_sensitive: false}

  before_create :set_channel

  def format_name
    "#{job_position.title} #{name}"
  end

  def self.online_status_list
    [:dinas, :cadangan, :lepas_dinas]
  end

  def is_non_active
    non_active_at.present?
  end

  def online_status_string
    if [:cadangan, "cadangan"].include?(online_status)
      "CADANGAN"
    elsif [:lepas_dinas, "lepas_dinas"].include?(online_status)
      "LEPAS DINAS"
    elsif [:dinas, "dinas"].include?(online_status)
      "DINAS"
    else
      self.online_status = :lepas_dinas
      save
      "LEPAS DINAS"
    end
  end

  private
  def set_channel
    unless self.online_status.present?
      self.online_status = :lepas_dinas
    end
    self.channel  = loop do
      seed = "--#{rand(10000)}--#{Time.zone.now}--#{self.nrp_number}--"
      ch   = Digest::SHA256.hexdigest(seed)[0, 16]
      break ch unless self.class.exists?(channel: ch)
    end
    self.password = self.nrp_number[self.nrp_number.length-5..self.nrp_number.length]
  end
end
