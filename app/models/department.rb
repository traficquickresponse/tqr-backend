class Department < ApplicationRecord
  has_many :admins
  has_many :users
  validates :title, presence: true
end
