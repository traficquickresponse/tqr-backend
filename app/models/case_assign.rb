class CaseAssign < ApplicationRecord
  belongs_to :user
  belongs_to :open_case
  validates :open_case_id, presence: true
  validates :user_id, presence: true

  def response_time
    if  checkin_at.present? && readed_at.present?
      second = checkin_at-readed_at
      if second <= 60
        "#{second.round} detik"
      elsif second > 60 && second < 3600
        "#{(second/60).round} menit #{(second % 60).round} detik"
      elsif second >= 3600
        "#{(second/3600).round} jam #{(second % 3600 / 60).round} menit #{((second % 3600) % 60).round} detik"
      end
    else
      "0"
    end
  end

  def is_checkin
    checkin_at.present?
  end

  def is_readed
    readed_at.present?
  end
end
