class OpenCase < ApplicationRecord
  belongs_to :case_type
  has_many :case_assigns, dependent: :destroy
  has_many :owner_cases, :through => :case_assigns, :source => 'user'
  validates :description, presence: true
  validates :location, presence: true
  validates :lat, presence: true
  validates :lng, presence: true
  validates :case_type_id, presence: true
  def is_case_close
    close_case_at.present?
  end
end
