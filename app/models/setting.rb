class Setting < ApplicationRecord
  def self.push params
    Notification.publish_channel({
                                   event:   "setting",
                                   data:    {setting: params[:data]},
                                   channel: "global_setting"})
  end
end
