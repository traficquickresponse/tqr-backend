class UserSetting < ApplicationRecord
  belongs_to :settingable, polymorphic: true

  def self.push params
    Notification.publish_message({
                                   event: "current_setting",
                                   data:  {setting: params[:data]},
                                   iid:   params[:iid_token]})
  end
end
