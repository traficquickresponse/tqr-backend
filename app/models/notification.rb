class Notification
  def self.publish_message params
    params       = {"collapse_key": params[:event],
                    "data":         params[:data],
                    "to":           params[:iid]
    }
    uri          = URI.parse('https://fcm.googleapis.com/fcm/send')
    http         = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request      = Net::HTTP::Post.new(uri.path,
                                       'Content-Type'  => 'application/json',
                                       'Authorization' => "key=#{ENV['FCM_SERVER_KEY']}")
    request.body = params.as_json.to_json
    response     = http.request(request)
    unless response.message.eql?("OK")
      return {status: "error", response: response}
    end
    puts response.body
  end

  def self.publish_channel params
    params       = {
      collapse_key: params[:event],
      to:           "/topics/#{params[:channel]}",
      data:         params[:data]
    }
    uri          = URI.parse('https://fcm.googleapis.com/fcm/send')
    http         = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request      = Net::HTTP::Post.new(uri.path,
                                       'Content-Type'  => 'application/json',
                                       'Authorization' => "key=#{ENV['FCM_SERVER_KEY']}")
    request.body = params.as_json.to_json
    puts request.body
    response     = http.request(request)
    unless response.message.eql?("OK")
      return {status: "error", response: response}
    end
    puts response.body
  end

end
