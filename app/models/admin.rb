class Admin < User
  def assign_user params
    user = User.find(params[:user_id])
    ca   = CaseAssign.find_by(user_id: params[:user_id], open_case_id: params[:open_case_id])
    if ca.present?
      return false
    end
    if user.present?
      ca              = user.case_assigns.new
      ca.open_case_id = params[:open_case_id]
      if ca.save(validate: false)
        Notification.publish_message({
                                       event: ca.open_case.case_type.event_name,
                                       data: {
                                         open_case: {
                                           id:          ca.open_case.id,
                                           location:    ca.open_case.location,
                                           description: ca.open_case.description,
                                           lat:         ca.open_case.lat,
                                           lng:         ca.open_case.lng,
                                           latlng:      "#{ca.open_case.lat},#{ca.open_case.lng}",
                                         }
                                       },
                                       iid: user.iid_token})
      else
        return false
      end
    else
      return false
    end
    ca
  end
end
