class CaseType < ApplicationRecord
  has_many :open_cases
  validates :title, presence: true
end
