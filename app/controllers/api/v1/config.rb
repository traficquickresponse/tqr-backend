module API
  module V1
    module Config
      extend ActiveSupport::Concern
      included do
        default_format :json
        version "v1", using: :accept_version_header, vendor: 'mnpix'
        format :json
        content_type :json, 'application/json; charset=UTF-8'
        formatter :json, API::V1::SuccessFormatter
        error_formatter :json, API::V1::ErrorFormatter

        rescue_from :all do |e|
          begin
            status = e.status
          rescue
            status = 500
          end
          error!(e, status)
        end
        helpers do
          include ActionController::HttpAuthentication::Token

          def me
            @me
          end

          def token
            token_params_from(headers['Authorization']).shift[1]
          end


          def authenticated_master
            headers['Authorization'] && ENV["MASTER_TOKEN"].eql?(token)
          end
          def authenticated_admin
            headers['Authorization'] && @me = Admin.find_by(authentication_token: token)
          end
          def authenticated_user
            headers['Authorization'] && @me = User.find_by(authentication_token: token, type: "User")
          end
          def authenticated_all
            headers['Authorization'] && @me = (User.find_by(authentication_token: token, type: "User") ||  Admin.find_by(authentication_token: token || ENV["MASTER_TOKEN"].eql?(token)))
          end
          def authenticated_admin_all
            headers['Authorization'] && @me = (Admin.find_by(authentication_token: token) || ENV["MASTER_TOKEN"].eql?(token))
          end

          def logger
            Rails.logger
          end


        end
      end
    end
  end
end
