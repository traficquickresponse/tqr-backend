class API::V1::HelloWorlds::Resources::HelloWorlds < Grape::API
  include API::V1::Config

  resource "pusher" do
    # desc "Push location status to admin" do
    #   headers "Authorization" => {
    #     description: "Token User",
    #     required:    true
    #   }
    # end
    # params do
    #   requires :lat, type: String
    #   requires :lng, type: String
    # end
    # post "/location_status" do
    #   error!("401 Unauthorized", 401) unless authenticated_user
    #   $push.trigger("admin_traffic_QR","location_status", {
    #     data: {
    #       uid: me.id,
    #       lat: params.lat,
    #       lng: params.lng
    #     }
    #   })
    #   {messages: "Success, check your subscribe channel #{params.channel}"}
    # end
  end

  # resource "development" do
  #   resource "Pusher" do
  #     desc "Push notification"
  #     params do
  #       requires :iid_token, type: String
  #       requires :event_name, type: String, default: CaseType.first.event_name, values: CaseType.all.map { |a| a.event_name }
  #       requires :messages, type: String
  #     end
  #     post "/push" do
  #       Notification.publish_message({
  #                                      event:  params.event_name,
  #                                      data: {
  #                                        open_case: {
  #                                          id:          "dummy-70a8-42d0-a14e-71dcd683a9d5",
  #                                          location:    "dummy jalan kaliurang km 12,5",
  #                                          description: params.messages,
  #                                          lat:         "dummy0.32456543234",
  #                                          lng:         "dummy-0.21345678654"
  #                                        }
  #                                      },
  #                                      iid: params.iid_token})
  #
  #       {messages: "Success, check your subscribe channel #{params.channel}"}
  #     end
  #   end
  # end
end
