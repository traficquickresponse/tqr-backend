class API::V1::OpenCases::Resources::OpenCases < Grape::API
  include API::V1::Config
  resource "open_cases" do
    desc "Get Cases (kejadian)" do
      headers "Authorization" => {
        description: "Token Admin or Token User",
        required:    true
      }
    end
    get "/" do
      unless authenticated_admin || authenticated_user
        error!("401 Unauthorized", 401)
      end
      kasus = []
      if authenticated_admin
        kasus = OpenCase.where(close_case_at: nil)
      end
      if authenticated_user
        kasus = me.my_cases.where(close_case_at: nil).includes(:case_assigns).where(case_assigns:{checkin_at: nil})
      end
      present :open_cases, kasus.order("open_cases.created_at DESC"), with: API::V1::OpenCases::Entities::OpenCase
    end

    desc "Close Case" do
      headers "Authorization" => {
        description: "Token Admin",
        required:    true
      }
    end
    params do
      requires :id, type: String, desc: "open_case_id"
    end
    delete "/closed" do
      error!("401 Unauthorized", 401) unless authenticated_admin
      oc = OpenCase.find_by(id: params.id)
      error!("Cant find open case with id '#{params.id}'", 401) unless oc.present?
      oc.close_case_at = Time.zone.now
      unless oc.save(validate: false)
        error!(oc.errors.full_messages.join(", "), 422)
      end
      present :open_cases, oc, with: API::V1::OpenCases::Entities::OpenCase
    end

    desc "Get Case (kejadian)" do
      headers "Authorization" => {
        description: "Token Admin or Token User",
        required:    true
      }
    end
    params do
      requires :id, type: String
    end
    get "/:id" do
      error!("401 Unauthorized", 401) unless authenticated_all
      kasus = []
      if me.type.eql?("Admin")
        kasus = OpenCase.find(params.id)
      end
      if me.type.eql?("User")
        ca = CaseAssign.find_by(user_id: me.id, open_case_id: params.id, checkin_at: nil)
        unless ca.present?
          error!("401 Unauthorized to view Case", 401)
        end
        kasus = ca.open_case
      end
      present :open_case, kasus, with: API::V1::OpenCases::Entities::OpenCase
    end
    desc "Checkin" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end
    params do
      requires :id, type: String, desc: "open_case_id"
      requires :description, type: String
    end
    put "/checkin" do
      error!("401 Unauthorized", 401) unless authenticated_user
      ca = CaseAssign.find_by(user_id: me.id, open_case_id: params.id)
      error!("Can't find id", 401) unless ca.present?
      error!("You have checkin!", 401) if ca.checkin_at.present?
      ca.checkin_at  = Time.zone.now.utc
      ca.description = params.description
      unless ca.save(validate: false)
        error!(ca.errors.full_messages.join(", "), 422)
      end
      present :quick_response, ca, with: API::V1::OpenCases::Entities::QuickResponse
    end
    desc "Readed" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end
    params do
      requires :id, type: String, desc: "open_case_id"
    end
    put "/readed" do
      error!("401 Unauthorized", 401) unless authenticated_user
      ca = CaseAssign.find_by(user_id: me.id, open_case_id: params.id)
      error!("Can't find id", 401) unless ca.present?
      error!("You have read this case!", 401) if ca.readed_at.present?
      ca.readed_at = Time.zone.now.utc
      unless ca.save(validate: false)
        error!(ca.errors.full_messages.join(", "), 422)
      end
      present :quick_response, ca, with: API::V1::OpenCases::Entities::QuickResponse
    end

    desc "Create Case (kejadian)" do
      headers "Authorization" => {
        description: "Token Admin",
        required:    true
      }
    end

    params do
      if ActiveRecord::Base.connection.table_exists? 'case_types'
        values = CaseType.all.map(&:title)
        value  = CaseType.all.map(&:title).first
      else
        values = ["No Case"]
        value  = "No Case"
      end
      requires :location, type: String
      requires :description, type: String
      requires :lat, type: BigDecimal
      requires :lng, type: BigDecimal
      requires :case_type, type: String, default: value, values: values
      requires :assign_user_ids, type: Array[String]
    end
    post "/" do
      error!("401 Unauthorized", 401) unless authenticated_admin
      ct = CaseType.find_by(title: params.case_type)
      unless ct.present?
        error!("Can't find CaseType!", 422)
      end
      kasus             = ct.open_cases.new
      kasus.location    = params.location
      kasus.description = params.description
      kasus.lat         = params.lat
      kasus.lng         = params.lng
      if kasus.save
        errors = []
        params.assign_user_ids.each do |id|
          if User.find_by(id: id)
            unless me.assign_user({user_id: id, open_case_id: kasus.id})
              errors << "Cant assign user id = '#{id}' "
            end
          else
            errors << "Cant find user by id = '#{id}' "
          end
        end
        if errors.present?
          error!("#{errors.join(", ")}, please try again for this user in endpoin case_assign!!.", 422)
        end
      else
        error!(kasus.errors.full_messages.join(", "), 422)
      end
      present :open_case, kasus, with: API::V1::OpenCases::Entities::OpenCase
    end
  end
end
