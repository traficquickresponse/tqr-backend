class API::V1::OpenCases::Entities::ExhibitionCase < Grape::Entity
  expose :id
  expose :location
  expose :description
  expose :lat
  expose :lng
  expose :is_case_close
  expose (:close_case_at) { |r| r.close_case_at.to_time.iso8601 if r.close_case_at.present? }
  expose (:created_at) { |r| r.created_at.to_time.iso8601 if r.created_at.present? }
  expose :case_type, using: API::V1::OpenCases::Entities::CaseType
end

