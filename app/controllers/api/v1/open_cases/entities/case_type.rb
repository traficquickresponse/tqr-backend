class API::V1::OpenCases::Entities::CaseType < Grape::Entity
  expose :event_name
  expose :title
end

