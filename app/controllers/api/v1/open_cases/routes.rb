module API
    module V1
      module OpenCases
        class Routes < Grape::API
          mount API::V1::OpenCases::Resources::OpenCases
        end
      end
  end
end
