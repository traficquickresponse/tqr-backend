module API
    module V1
      module Departments
        class Routes < Grape::API
          mount API::V1::Departments::Resources::Departments
        end
      end
  end
end
