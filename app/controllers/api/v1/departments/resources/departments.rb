class API::V1::Departments::Resources::Departments < Grape::API
  include API::V1::Config
  resource "departments" do
    desc "Get Departments" do
      detail ': Get info Departments'
        # headers "Authorization" => {
        #         description: "Token Admin",
        #         required:    true
        #     }
    end
    get "/" do
      #error!("401 Unauthorized", 401) unless authenticated_admin
      departments = Department.all
      present :departments, departments, with: API::V1::Departments::Entities::Department
    end
    desc "Get Departments by id" do
      detail ': Get info departement'
        # headers "Authorization" => {
        #         description: "Token Admin",
        #         required:    true
        #     }
    end
    params do
      requires :id, type: String
    end
    get "/:id" do
      #error!("401 Unauthorized", 401) unless authenticated_admin
      department = Department.find(params.id)
      present :department, department, with: API::V1::Departments::Entities::Department
    end

    desc "Create Job Departments", {
        headers: {
            "Authorization" => {
                description: "Token Master",
                required:    true
            }
        }
    }
    params do
      requires :title, type: String
    end
    post "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      all = Department.all.map{|c| c.title.upcase}
      if all.include?(params.title.upcase)
        error!("Duplikat departments!!", 422)
      end
      jp = Department.new
      jp.title = params.title.upcase
      unless jp.save
        error!(jp.errors.full_messages.join(", "), 422)
      end
      present :department, jp, with: API::V1::Departments::Entities::Department
    end

    desc "Update Job Position", {
      headers: {
        "Authorization" => {
          description: "Token Master",
          required:    true
        }
      }
    }
    params do
      requires :id, type: String
      requires :title, type: String
    end
    put "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      all = Department.all.map{|c| c.title.upcase}
      if all.include?(params.title.upcase)
        error!("Duplikat department!!", 422)
      end
      jp = Department.find_by(id: params.id)
      error!("Cant find Department!!", 422) unless jp
      jp.title = params.title.upcase
      unless jp.save
        error!(jp.errors.full_messages.join(", "), 422)
      end
      present :department, jp, with: API::V1::Departments::Entities::Department
    end

    desc "Delete Department", {
      headers: {
        "Authorization" => {
          description: "Token Master",
          required:    true
        }
      }
    }
    params do
      requires :id, type: String
    end
    delete "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      jp = Department.find_by(id: params.id)
      error!("Cant find Department!!", 422) unless jp
      error!("Cant delete Department!!, User have used this Department.", 422) if jp.users.present?
      error!("Cant delete Department!!, Admin have used this Department.", 422) if jp.admins.present?
      unless jp.destroy
        error!(jp.errors.full_messages.join(", "), 422)
      end
     {message: "Success destroy Department."}
    end
  end
end
