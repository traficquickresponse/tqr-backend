require "grape-swagger"
module API
  module V1
    class Main < Grape::API
      use Grape::Middleware::Logger, {
          logger: Logger.new(STDERR)
      }
      mount API::V1::HelloWorlds::Routes
      mount API::V1::JobPositions::Routes
      mount API::V1::Departments::Routes
      mount API::V1::Admins::Routes
      mount API::V1::Users::Routes
      mount API::V1::OpenCases::Routes
      mount API::V1::CaseAssigns::Routes

      # swagger settings
      options = {version: "v1"}
      add_swagger_documentation(
          api_version:             options[:version],
          doc_version:             options[:version],
          hide_documentation_path: true,
          mount_path:              "documentation/#{options[:version]}/doc",
          hide_format:             true
      )
    end
  end
end