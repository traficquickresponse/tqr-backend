class API::V1::Admins::Entities::Admin < Grape::Entity
  expose :id
  expose :name
  expose :phone_number
  expose :format_name
  expose :nrp_number
  expose :channel
  expose :is_online
  expose :job_position, using: API::V1::JobPositions::Entities::JobPosition
  expose :department, using: API::V1::Departments::Entities::Department
end

