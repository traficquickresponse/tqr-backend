module API
  module V1
    module Admins
      class Routes < Grape::API
        mount API::V1::Admins::Resources::Admins
      end
    end
  end
end
