class API::V1::Admins::Resources::Admins < Grape::API
  include API::V1::Config
  desc "Get Admin"
  resource "admins" do
    desc "Get Admins" do
      headers "Authorization" => {
          description: "Token Admin_Master",
          required:    true
      }
    end
    get "/" do
      error!("401 Unauthorized", 401) unless authenticated_admin_all
      present :admins, Admin.all, with: API::V1::Admins::Entities::Admin
    end

    desc "Get Admin" do
      headers "Authorization" => {
          description: "Token Admin_Master",
          required:    true
      }
    end
    get "/current" do
      error!("401 Unauthorized", 401) unless authenticated_admin_all
      present :admin, me, with: API::V1::Admins::Entities::Admin
    end

    resource "session" do
      desc "Create Admin" do
        headers "Authorization" => {
            description: "Token Master",
            required:    true
        }
      end
      params do
        requires :user_id, type: String
      end
      post "assign" do
        error!("401 Unauthorized", 401) unless authenticated_master
        admin                       = User.find_by(id:  params.user_id)
        unless admin
          error!("Cant find user with id '#{params.user_id}'", 422)
        end
        admin.type                  = "Admin"
        unless admin.save(validate: false)
          error!(admin.errors.full_messages.join(", "), 422)
        end
        {authentication_token: admin.authentication_token}
      end

      desc "Destroy Admin" do
        headers "Authorization" => {
            description: "Token Master",
            required:    true
        }
      end
      params do
        requires :admin_id, type: String
      end
      delete "assign" do
        error!("401 Unauthorized", 401) unless authenticated_master
        admin                       = Admin.find_by(id: params.admin_id)
        unless admin
          error!("Cant find user with id '#{params.admin_id}'", 422)
        end
        admin.type                  = "User"
        unless admin.save(validate: false)
          error!(admin.errors.full_messages.join(", "), 422)
        end
        {authentication_token: admin.authentication_token}
      end

      desc "Login Admin"
      params do
        requires :nrp_number, type: String
        requires :password, type: String
      end
      post "sign_in" do
        admin = Admin.find_by(nrp_number: params.nrp_number)
        error!("Cant find Admin with nrp: '#{params.nrp_number}' ", 401) unless admin.present?
        error!("401 Unauthorized", 401) unless admin.authenticate(params.password)
        {authentication_token: admin.authentication_token}
      end
      desc "Update password Admin" do
        headers "Authorization" => {
            description: "Token Admin",
            required:    true
        }
      end
      params do
        requires :old_password, type: String
        requires :password, type: String
        requires :password_confirmation, type: String
      end
      put "password" do
        error!("401 Unauthorized", 401) unless authenticated_admin
        error!("401 Unauthorized", 401) unless me.authenticate(params.old_password)
        me.password             = params.password
        me.password_confirmation= params.password_confirmation
        unless me.save
          error!(me.errors.full_messages.join(", "), 422)
        end
        {message: "Success updated Password"}
      end
    end
  end
end
