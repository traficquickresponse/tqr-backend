class API::V1::CaseAssigns::Entities::CaseAssign < Grape::Entity
  expose :id
  expose :user_id
  expose :open_case, using: API::V1::OpenCases::Entities::OpenCase
end

