module API
    module V1
      module CaseAssigns
        class Routes < Grape::API
          mount API::V1::CaseAssigns::Resources::CaseAssigns
        end
      end
  end
end
