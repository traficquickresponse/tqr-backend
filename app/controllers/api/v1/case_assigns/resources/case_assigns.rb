class API::V1::CaseAssigns::Resources::CaseAssigns < Grape::API
  include API::V1::Config
  resource "case_assigns" do
    desc "Assign case" do
      headers "Authorization" => {
          description: "Token Admin",
          required:    true
      }
    end
    params do
      requires :user_id, type: String
      requires :open_case_id, type: String
    end
    post "/" do
      error!("401 Unauthorized", 401) unless authenticated_admin
      assign = me.assign_user({user_id: params.user_id, open_case_id: params.open_case_id})
      error!("Failed to assign!!", 401) unless assign
      present :case_assign, assign, with: API::V1::CaseAssigns::Entities::CaseAssign
    end

    desc "UnAssign case" do
      headers "Authorization" => {
          description: "Token Admin",
          required:    true
      }
    end
    params do
      requires :user_id, type: String
      requires :open_case_id, type: String
    end
    delete "/" do
      error!("401 Unauthorized", 401) unless authenticated_admin
      assign = me.unassign_user({user_id: params.user_id, open_case_id: params.open_case_id})
      error!("Failed to unassign!!", 401) unless assign
      {message: "Un assign case success."}
    end


  end
end
