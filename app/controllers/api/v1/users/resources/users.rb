class API::V1::Users::Resources::Users < Grape::API
  include API::V1::Config
  resource "masters" do
    desc "Login Super User"
    params do
      requires :username, type: String
      requires :password, type: String
    end
    post "sign_in" do
      error!("Username not valid", 401) unless ENV["MASTER_USERNAME"].eql?(params.username)
      error!("Password not valid", 401) unless ENV["MASTER_PASSWORD"].eql?(params.password)
      {user: {authentication_token: ENV["MASTER_TOKEN"]}}
    end

    resource "settings" do
      desc "Update Settings" do
        headers "Authorization" => {
          description: "Token admin",
          required:    true
        }
      end
      params do
        requires :update_interval, type: Integer
        requires :update_radius, type: Integer
        requires :is_monitoring, type: Boolean
      end
      put "/" do
        error!("401 Unauthorized", 401) unless authenticated_admin
        setting                 = Setting.first
        setting.update_interval = params.update_interval
        setting.update_radius   = params.update_radius
        setting.is_monitoring   = params.is_monitoring
        unless setting.save
          error!(setting.errors.full_messages.join(", "), 422)
        end
        Setting.push({data: {
          update_interval: setting.update_interval,
          update_radius:   setting.update_radius,
          is_monitoring:   setting.is_monitoring
        }})
        present :setting, setting, with: API::V1::Users::Entities::Setting
      end
    end
  end

  resource "users" do
    desc "Get All Users" do
      headers "Authorization" => {
        description: "Token Admin_Master",
        required:    true
      }
    end
    get "/" do
      error!("401 Unauthorized", 401) unless authenticated_admin_all
      users = User.where(type: :User)
      present :users, users, with: API::V1::Users::Entities::User
    end
    desc "Get All Users in pergelaran" do
      headers "Authorization" => {
        description: "Token Admin",
        required:    true
      }
    end
    get "/exhibition" do
      error!("401 Unauthorized", 401) unless authenticated_admin
      users = User.where(type: :User)
      present :users, users, with: API::V1::Users::Entities::User
    end

    desc "Get User" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end

    desc "Update User" do
      headers "Authorization" => {
        description: "Token Master",
        required:    true
      }
    end
    params do
      requires :user_id, type: String
      requires :name, type: String
      requires :nrp_number, type: String
      requires :phone_number, type: String, desc: "ex: +62xxxxxxxxxx"
      requires :job_position_id, type: String
      requires :department_id, type: String
    end
    put "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      unless Department.find_by(id: params.department_id)
        error!("Cant find Department with id '#{params.department_id}'", 422)
      end
      unless JobPosition.find_by(id: params.job_position_id)
        error!("Cant find Job Position with id '#{params.job_position_id}'", 422)
      end
      user = User.find(params.user_id)
      unless user
        error!("Cant find User with id '#{params.user_id}'", 422)
      end

      user.job_position_id       = params.job_position_id
      user.department_id         = params.department_id
      user.name                  = params.name
      user.phone_number          = params.phone_number
      user.nrp_number            = params.nrp_number
      user.password              = params.nrp_number[params.nrp_number.length-5..params.nrp_number.length]
      user.password_confirmation = params.nrp_number[params.nrp_number.length-5..params.nrp_number.length]
      unless user.save
        error!(user.errors.full_messages.join(", "), 422)
      end
      present :user, user, with: API::V1::Users::Entities::User
    end
    desc "Get current" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end
    get "/current" do
      error!("401 Unauthorized", 401) unless authenticated_user
      present :user, me, with: API::V1::Users::Entities::User
    end

    desc "Get User by Id" do
      headers "Authorization" => {
        description: "Token All",
        required:    true
      }
    end
    params do
      requires :id, type: String
    end
    get "/:id" do
      error!("401 Unauthorized", 401) unless authenticated_all
      user = User.find(params.id)
      present :user, user, with: API::V1::Users::Entities::User
    end

    desc "Change Status Online" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end
    params do
      requires :online_status, type: String, default: User.online_status_list.first.to_s, values: User.online_status_list.map { |a| a.to_s }
    end
    post "/online_status" do
      error!("401 Unauthorized", 401) unless authenticated_user
      me.online_status = params.online_status
      unless me.save(validate: false)
        error!(me.errors.full_messages.join(", "), 422)
      end
      {message: "success change status to #{me.online_status_string}"}
    end

    desc "Update location by mobile" do
      headers "Authorization" => {
        description: "Token User",
        required:    true
      }
    end
    params do
      requires :lat, type: String
      requires :lng, type: String
    end
    put "/location" do
      error!("401 Unauthorized", 401) unless authenticated_user
      me.lat = params.lat
      me.lng = params.lng
      unless me.save(validate: false)
        error!(me.errors.full_messages.join(", "), 422)
      end
      {message: "success update location"}
    end

    desc "Non Active User" do
      headers "Authorization" => {
        description: "Token Master",
        required:    true
      }
    end
    params do
      requires :id, type: String, desc: "User id"
    end
    delete "/non_active" do
      error!("401 Unauthorized", 401) unless authenticated_master
      user = User.find(params.id)
      error!("Cant find user", 401) unless user.present?
      user.non_active_at = Time.zone.now
      user.is_online     = false
      unless user.save(validate: false)
        error!(user.errors.full_messages.join(", "), 422)
      end
      {message: "success non active user"}
    end

    desc "Active User" do
      headers "Authorization" => {
        description: "Token Master",
        required:    true
      }
    end
    params do
      requires :id, type: String, desc: "User id"
    end
    put "/active" do
      error!("401 Unauthorized", 401) unless authenticated_master
      user = User.find(params.id)
      error!("Cant find user", 401) unless user.present?
      user.non_active_at = nil
      user.is_online     = false
      unless user.save(validate: false)
        error!(user.errors.full_messages.join(", "), 422)
      end
      {message: "success actived user"}
    end

    resource "session" do
      desc "Create User" do
        headers "Authorization" => {
          description: "Token Master",
          required:    true
        }
      end
      params do
        requires :name, type: String
        requires :nrp_number, type: String
        requires :phone_number, type: String, desc: "ex: +62xxxxxxxxxx"
        requires :job_position_id, type: String
        requires :department_id, type: String
      end
      post "sign_up" do
        error!("401 Unauthorized", 401) unless authenticated_master
        unless Department.find_by(id: params.department_id)
          error!("Cant find Department with id '#{params.department_id}'", 422)
        end
        user                       = JobPosition.find(params.job_position_id).users.new
        user.department_id         = params.department_id
        user.name                  = params.name
        user.phone_number          = params.phone_number
        user.nrp_number            = params.nrp_number
        user.password              = params.nrp_number[params.nrp_number.length-5..params.nrp_number.length]
        user.password_confirmation = params.nrp_number[params.nrp_number.length-5..params.nrp_number.length]
        unless user.save
          error!(user.errors.full_messages.join(", "), 422)
        end
        {user: {authentication_token: user.authentication_token, channel: user.channel, iid_token: user.iid_token}}
      end

      desc "Login User"
      params do
        requires :nrp_number, type: String
        requires :password, type: String
        requires :iid_token, type: String
      end
      post "sign_in" do
        me = User.find_by(nrp_number: params.nrp_number)
        error!("iid_token not valid!", 401) if params.iid_token.blank? || params.iid_token.size <= 10
        error!("401 Unauthorized", 401) unless me.authenticate(params.password)
        error!("User Non Active call your admin!!", 403) if me.is_non_active
        me.is_online = true
        me.iid_token = params.iid_token
        error!("Login failed", 403) unless me.save(validate: false)
        {user: {authentication_token: me.authentication_token, channel: me.channel, iid_token: me.iid_token}}
      end
      desc "On refresh Token" do
        headers "Authorization" => {
          description: "Token User",
          required:    true
        }
      end
      params do
        requires :iid_token, type: String
      end
      put "refresh" do
        error!("401 Unauthorized", 401) unless authenticated_user
        me.iid_token = params.iid_token
        error!("Fail to refresh token", 403) unless me.save(validate: false)
        {user: {authentication_token: me.authentication_token, channel: me.channel, iid_token: me.iid_token}}
      end
      desc "Logout User" do
        headers "Authorization" => {
          description: "Token User",
          required:    true
        }
      end
      post "sign_out" do
        error!("401 Unauthorized", 401) unless authenticated_user
        me.is_online = false
        me.save(validate: false)
        {message: "Success Logout"}
      end

      desc "Update password User" do
        headers "Authorization" => {
          description: "Token User",
          required:    true
        }
      end
      params do
        requires :old_password, type: String
        requires :password, type: String
        requires :password_confirmation, type: String
      end
      put "password" do
        error!("401 Unauthorized", 401) unless authenticated_user
        error!("401 Unauthorized", 401) unless me.authenticate(params.old_password)
        me.password             = params.password
        me.password_confirmation= params.password_confirmation
        unless me.save
          error!(me.errors.full_messages.join(", "), 422)
        end
        {message: "Success updated Password"}
      end
    end
    resource "settings" do
      desc "Update Settings" do
        headers "Authorization" => {
          description: "Token Admin",
          required:    true
        }
      end
      params do
        requires :update_interval, type: Integer
        requires :update_radius, type: Integer
        requires :is_monitoring, type: Boolean
        requires :user_id, type: Integer
      end
      put "/" do
        error!("401 Unauthorized", 401) unless authenticated_admin
        me = User.find(params.user_id)
        error!("User not found!", 401) unless me.present?
        if me.user_setting.present?
          setting = me.user_setting
        else
          setting = me.build_user_setting
        end
        setting.update_interval = params.update_interval
        setting.update_radius   = params.update_radius
        setting.is_monitoring   = params.is_monitoring
        unless setting.save
          error!(me.errors.full_messages.join(", "), 422)
        end
        UserSetting.push({data: {
          update_interval: me.user_setting.update_interval,
          update_radius:   me.user_setting.update_radius,
          is_monitoring:   me.user_setting.is_monitoring
        }, iid_token:           me.iid_token})

        present :user, me, with: API::V1::Users::Entities::User
      end
    end
  end
end
