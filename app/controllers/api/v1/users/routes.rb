module API
  module V1
    module Users
      class Routes < Grape::API
        mount API::V1::Users::Resources::Users
      end
    end
  end
end
