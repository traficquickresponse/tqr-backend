class API::V1::Users::Entities::QuickResponse < Grape::Entity
  expose (:checkin_at) { |r| r.checkin_at.to_time.iso8601 if r.checkin_at.present? }
  expose (:readed_at) { |r| r.readed_at.to_time.iso8601 if r.readed_at.present? }
  expose :description
  expose :is_checkin
  expose :is_readed
  expose :response_time
  expose :open_case, using: API::V1::Users::Entities::OpenCase
end

