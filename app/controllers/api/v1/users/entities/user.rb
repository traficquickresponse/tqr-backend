class API::V1::Users::Entities::User < Grape::Entity
  expose :id
  expose :name
  expose :format_name
  expose :nrp_number
  expose :phone_number
  expose :channel
  expose :iid_token
  expose :is_online
  expose :online_status
  expose :online_status_string
  expose (:non_active_at) { |r| r.non_active_at.to_time.iso8601 if r.non_active_at.present? }
  expose :is_non_active
  expose :lat
  expose :lng
  expose :job_position, using: API::V1::JobPositions::Entities::JobPosition
  expose :setting, using: API::V1::Users::Entities::Setting
  expose :department, using: API::V1::Departments::Entities::Department
  #expose :case_assigns, as: :checkins, using: API::V1::Users::Entities::QuickResponse
end

