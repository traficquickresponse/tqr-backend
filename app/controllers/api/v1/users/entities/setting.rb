class API::V1::Users::Entities::Setting < Grape::Entity
  expose :update_interval
  expose :update_radius
  expose :is_monitoring
end

