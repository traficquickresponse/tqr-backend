class API::V1::JobPositions::Resources::JobPositions < Grape::API
  include API::V1::Config
  resource "job_positions" do
    desc "Get Position" do
      detail ': Get info position'
        # headers "Authorization" => {
        #         description: "Token Admin",
        #         required:    true
        #     }
    end
    get "/" do
      #error!("401 Unauthorized", 401) unless authenticated_admin
      jp = JobPosition.all
      present :job_positions, jp, with: API::V1::JobPositions::Entities::JobPosition
    end
    desc "Get Position by id" do
      detail ': Get info position'
        # headers "Authorization" => {
        #         description: "Token Admin",
        #         required:    true
        #     }
    end
    params do
      requires :id, type: String
    end
    get "/:id" do
      #error!("401 Unauthorized", 401) unless authenticated_admin
      jp = JobPosition.find(params.id)
      present :job_position, jp, with: API::V1::JobPositions::Entities::JobPosition
    end

    desc "Create Job Position", {
        headers: {
            "Authorization" => {
                description: "Token Master",
                required:    true
            }
        }
    }
    params do
      requires :title, type: String
    end
    post "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      all = JobPosition.all.map{|c| c.title.upcase}
      if all.include?(params.title.upcase)
        error!("Duplikat job_position!!", 422)
      end
      jp = JobPosition.new
      jp.title = params.title.upcase
      unless jp.save
        error!(jp.errors.full_messages.join(", "), 422)
      end
      present :job_position, jp, with: API::V1::JobPositions::Entities::JobPosition
    end

    desc "Update Job Position", {
      headers: {
        "Authorization" => {
          description: "Token Master",
          required:    true
        }
      }
    }
    params do
      requires :id, type: String
      requires :title, type: String
    end
    put "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      all = JobPosition.all.map{|c| c.title.upcase}
      if all.include?(params.title.upcase)
        error!("Duplikat job_position!!", 422)
      end
      jp = JobPosition.find_by(id: params.id)
      error!("Cant find job_position!!", 422) unless jp
      jp.title = params.title.upcase
      unless jp.save
        error!(jp.errors.full_messages.join(", "), 422)
      end
      present :job_position, jp, with: API::V1::JobPositions::Entities::JobPosition
    end
    desc "Delete Job Position", {
      headers: {
        "Authorization" => {
          description: "Token Master",
          required:    true
        }
      }
    }
    params do
      requires :id, type: String
    end
    delete "/" do
      error!("401 Unauthorized", 401) unless authenticated_master
      jp = JobPosition.find_by(id: params.id)
      error!("Cant find Department!!", 422) unless jp
      error!("Cant delete Department!!, User have used this Job Position.", 422) if jp.users.present?
      error!("Cant delete Department!!, Admin have used this Job Position.", 422) if jp.admins.present?
      unless jp.destroy
        error!(jp.errors.full_messages.join(", "), 422)
      end
      {message: "Success destroy Job Position."}
    end
  end
end
