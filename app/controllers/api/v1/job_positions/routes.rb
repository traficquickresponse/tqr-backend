module API
    module V1
      module JobPositions
        class Routes < Grape::API
          mount API::V1::JobPositions::Resources::JobPositions
        end
      end
  end
end
