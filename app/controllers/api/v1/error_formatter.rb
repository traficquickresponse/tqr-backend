module API
  module V1
    module ErrorFormatter
      def self.call message, backtrace, options, env
        {
            error:{
                errors: [message]
            }
        }.to_json
      end
    end
  end
end